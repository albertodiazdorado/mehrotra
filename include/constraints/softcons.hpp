#ifndef SOFT_CONS_HPP_
#define SOFT_CONS_HPP_

#include <algorithm>
#include <cmath>

#include "matrix/matrix.hpp"

namespace mehrotra {
namespace softcons {

template <class float_t, class int_t, size_t n, size_t k, size_t T>
class unconstrained {
private:
protected:
  unconstrained() = default;

  unconstrained(std::array<float_t, n> ub, std::array<float_t, n> lb,
                GeMat<float_t, k, n> F, std::array<float_t, k> f) {}

  void initialize(std::array<float_t, n> ub, std::array<float_t, n> lb,
                  GeMat<float_t, k, n> F, std::array<float_t, k> f) {}

  void new_x0(const float_t) { return; }

  template <template <class, size_t> class matrix>
  void afs(const std::array<float_t, n> &x, matrix<float_t, n> &phi,
           std::array<float_t, n> &rd, const size_t step = 0,
           const std::false_type = std::false_type{}) {
    detail::MatOps<float_t>::cholfact(phi);
  }

  template <template <class, size_t> class matrix>
  void afs(const std::array<float_t, n> &x, matrix<float_t, n> &phi,
           std::array<float_t, n> &rd, const size_t step = 0,
           const std::true_type = std::true_type{}) {}

public:
  void update(const float_t) { return; }
  void set_rho0(const float_t) {}
};

template <class float_t, class int_t, size_t n, size_t k, size_t T>
class simplebounds {
private:
  std::array<float_t, n> ub{}, lb{};
  std::array<std::array<float_t, n>, T> eu{}, el{};
  std::array<std::array<float_t, n>, T> dgu{}, dgl{}, dhu{}, dhl{};
  float_t rho;

#ifdef MPC_GTEST_COMPILE
public:
#else
protected:
#endif
  simplebounds() = default;

  simplebounds(std::array<float_t, n> ub, std::array<float_t, n> lb,
               GeMat<float_t, k, n>, std::array<float_t, k>)
      : simplebounds(std::move(ub), std::move(lb)) {}

  simplebounds(std::array<float_t, n> _ub, std::array<float_t, n> _lb)
      : ub{std::move(_ub)}, lb{std::move(_lb)} {}

  void initialize(std::array<float_t, n> _ub, std::array<float_t, n> _lb,
                  GeMat<float_t, k, n>, std::array<float_t, k>) {
    ub = std::move(_ub);
    lb = std::move(_lb);
  }

  void new_x0(const float_t rho0) { rho = rho0; }

  template <template <class, size_t> class matrix>
  void afs(const std::array<float_t, n> &x, matrix<float_t, n> &phi,
           std::array<float_t, n> &rd, const size_t step,
           const std::false_type = std::false_type{}) {
    // Evaluate the error
    std::transform(x.cbegin(), x.cend(), ub.cbegin(), eu[step].begin(),
                   std::minus<float_t>());
    std::transform(lb.cbegin(), lb.cend(), x.cbegin(), el[step].begin(),
                   std::minus<float_t>());

    // Evaluate dg and dh
    for (size_t idx = 0; idx < n; idx++) {
      if (eu[step][idx] < 0) {
        eu[step][idx] = std::exp(rho * eu[step][idx]);
        dgu[step][idx] = eu[step][idx] / (1 + eu[step][idx]);
      } else {
        eu[step][idx] = std::exp(-rho * eu[step][idx]);
        dgu[step][idx] = 1 / (1 + eu[step][idx]);
      }
      dhu[step][idx] =
          rho * eu[step][idx] / (1 + eu[step][idx]) / (1 + eu[step][idx]);
    }

    for (size_t idx = 0; idx < n; idx++) {
      if (el[step][idx] < 0) {
        el[step][idx] = std::exp(rho * el[step][idx]);
        dgl[step][idx] = el[step][idx] / (1 + el[step][idx]);
      } else {
        el[step][idx] = std::exp(-rho * el[step][idx]);
        dgl[step][idx] = 1 / (1 + el[step][idx]);
      }
      dhl[step][idx] =
          rho * el[step][idx] / (1 + el[step][idx]) / (1 + el[step][idx]);
    }

    // Augment phi
    detail::MatOps<float_t>::diagsum(phi, dhu[step], dhl[step]);

    // Augment rd
    for (size_t idx = 0; idx < n; idx++) {
      rd[idx] += dgu[step][idx] - dgl[step][idx];
    }

    detail::MatOps<float_t>::cholfact(phi);
  }

public:
  void update(const float_t rho_update) { rho *= rho_update; }
  void set_rho0(const float_t rho0) { rho = rho0; }
};

template <class float_t, class int_t, size_t n, size_t k, size_t T>
class linearbounds {
private:
  GeMat<float_t, k, n> F{};
  std::array<float_t, k> f{};
  std::array<std::array<float_t, k>, T> e{};
  std::array<std::array<float_t, k>, T> dg{}, dh{};
  float_t rho;

#ifdef MPC_GTEST_COMPILE
public:
#else
protected:
#endif
  linearbounds() = default;

  linearbounds(std::array<float_t, n> ub, std::array<float_t, n> lb,
               GeMat<float_t, k, n> x, std::array<float_t, k> y)
      : F(std::move(x)), f{std::move(y)} {}

  void initialize(std::array<float_t, n> _ub, std::array<float_t, n> _lb,
                  GeMat<float_t, k, n> _F, std::array<float_t, k> _f) {
    F = std::move(_F);
    f = std::move(_f);
  }

  void new_x0(const float_t rho0) { rho = rho0; }

  void afs(const std::array<float_t, n> &x, PDMat<float_t, n> &phi,
           std::array<float_t, n> &rd, const size_t step,
           const std::false_type = std::false_type{}) {
    // Evaluate the error
    e[step] = f;
    detail::MatOps<float_t>::template matvec<'N'>(float_t{1}, F, x, float_t{-1},
                                                  e[step]);

    // Evaluate dg and dh
    for (size_t idx = 0; idx < n; idx++) {
      if (e[step][idx] < 0) {
        e[step][idx] = std::exp(rho * e[step][idx]);
        dg[step][idx] = e[step][idx] / (1 + e[step][idx]);
      } else {
        e[step][idx] = std::exp(-rho * e[step][idx]);
        dg[step][idx] = 1 / (1 + e[step][idx]);
      }
      dh[step][idx] =
          rho * e[step][idx] / (1 + e[step][idx]) / (1 + e[step][idx]);
    }

    // Augment phi
    detail::MatOps<float_t>::rankkt(dh[step], phi, F);

    // Augment rd
    detail::MatOps<float_t>::template matvec<'T'>(float_t{1}, F, dg[step],
                                                  float_t{1}, rd);

    detail::MatOps<float_t>::cholfact(phi);
  }

  void afs(const std::array<float_t, n> &x, PDMat<float_t, n> &phi,
           std::array<float_t, n> &rd, const size_t step,
           const std::true_type = std::true_type{}) {
    // Evaluate the error
    e[step] = f;
    detail::MatOps<float_t>::template matvec<'N'>(float_t{1}, F, x, float_t{-1},
                                                  e[step]);

    // Evaluate dg and dh
    for (size_t idx = 0; idx < n; idx++) {
      if (e[step][idx] < 0) {
        e[step][idx] = std::exp(rho * e[step][idx]);
        dg[step][idx] = e[step][idx] / (1 + e[step][idx]);
      } else {
        e[step][idx] = std::exp(-rho * e[step][idx]);
        dg[step][idx] = 1 / (1 + e[step][idx]);
      }
      dh[step][idx] =
          rho * e[step][idx] / (1 + e[step][idx]) / (1 + e[step][idx]);
    }

    // Augment phi
    {
      std::array<float_t, n> aux;
      for (size_t row = 0; row < k; row++) {
        for (size_t idx = 0; idx < n; idx++) {
          aux[idx] = std::sqrt(dh[step][row]) * F.mat[row + k * idx];
        }
        detail::MatOps<float_t>::cholupdate(phi, aux);
      }
    }

    // Augment rd
    detail::MatOps<float_t>::template matvec<'T'>(float_t{1}, F, dg[step],
                                                  float_t{1}, rd);

    // Do not factorize
  }

public:
  void update(const float_t rho_update) { rho *= rho_update; }
  void set_rho0(const float_t rho0) { rho = rho0; }
};

template <class float_t, class int_t, size_t n, size_t k, size_t T>
class fullbounds {
private:
  // Space for linear constraints
  GeMat<float_t, k, n> F{};
  std::array<float_t, k> f{};
  std::array<std::array<float_t, k>, T> e{};
  std::array<std::array<float_t, k>, T> dg{}, dh{};

  // Space for soft constraints
  std::array<float_t, n> ub{}, lb{};
  std::array<std::array<float_t, n>, T> eu{}, el{};
  std::array<std::array<float_t, n>, T> dgu{}, dgl{}, dhu{}, dhl{};

  // Others
  float_t rho;

#ifdef MPC_GTEST_COMPILE
public:
#else
protected:
#endif
  fullbounds() = default;

  fullbounds(std::array<float_t, n> _ub, std::array<float_t, n> _lb,
             GeMat<float_t, k, n> _F, std::array<float_t, k> _f)
      : ub(std::move(_ub)), lb(std::move(_lb)),
        F(std::move(_F)), f{std::move(_f)} {}

  void initialize(std::array<float_t, n> _ub, std::array<float_t, n> _lb,
                  GeMat<float_t, k, n> _F, std::array<float_t, k> _f) {
    ub = std::move(_ub);
    lb = std::move(_lb);
    F = std::move(_F);
    f = std::move(_f);
  }

  void new_x0(const float_t rho0) { rho = rho0; }
  
  void afs(const std::array<float_t, n> &x, PDMat<float_t, n> &phi,
           std::array<float_t, n> &rd, const size_t step,
           const std::false_type = std::false_type{}) {
    // Evaluate the error: linear bounds
    e[step] = f;
    detail::MatOps<float_t>::template matvec<'N'>(float_t{1}, F, x, float_t{-1},
                                                  e[step]);

    // Evaluate the error: simple bounds
    std::transform(x.cbegin(), x.cend(), ub.cbegin(), eu[step].begin(),
                   std::minus<float_t>());
    std::transform(lb.cbegin(), lb.cend(), x.cbegin(), el[step].begin(),
                   std::minus<float_t>());

    // Evaluate dg and dh: linear cons
    for (size_t idx = 0; idx < n; idx++) {
      if (e[step][idx] < 0) {
        e[step][idx] = std::exp(rho * e[step][idx]);
        dg[step][idx] = e[step][idx] / (1 + e[step][idx]);
      } else {
        e[step][idx] = std::exp(-rho * e[step][idx]);
        dg[step][idx] = 1 / (1 + e[step][idx]);
      }
      dh[step][idx] =
          rho * e[step][idx] / (1 + e[step][idx]) / (1 + e[step][idx]);
    }

    // Evaluate dg and dh: simple bounds
    for (size_t idx = 0; idx < n; idx++) {
      if (eu[step][idx] < 0) {
        eu[step][idx] = std::exp(rho * eu[step][idx]);
        dgu[step][idx] = eu[step][idx] / (1 + eu[step][idx]);
      } else {
        eu[step][idx] = std::exp(-rho * eu[step][idx]);
        dgu[step][idx] = 1 / (1 + eu[step][idx]);
      }
      dhu[step][idx] =
          rho * eu[step][idx] / (1 + eu[step][idx]) / (1 + eu[step][idx]);
    }

    for (size_t idx = 0; idx < n; idx++) {
      if (el[step][idx] < 0) {
        el[step][idx] = std::exp(rho * el[step][idx]);
        dgl[step][idx] = el[step][idx] / (1 + el[step][idx]);
      } else {
        el[step][idx] = std::exp(-rho * el[step][idx]);
        dgl[step][idx] = 1 / (1 + el[step][idx]);
      }
      dhl[step][idx] =
          rho * el[step][idx] / (1 + el[step][idx]) / (1 + el[step][idx]);
    }

    // Augment phi
    detail::MatOps<float_t>::diagsum(phi, dhu[step], dhl[step]);
    detail::MatOps<float_t>::rankkt(dh[step], phi, F);

    // Augment rd
    for (size_t idx = 0; idx < n; idx++) {
      rd[idx] += dgu[step][idx] - dgl[step][idx];
    }
    detail::MatOps<float_t>::template matvec<'T'>(float_t{1}, F, dg[step],
                                                  float_t{1}, rd);

    // Factorize
    detail::MatOps<float_t>::cholfact(phi);
  }

public:
  void update(const float_t rho_update) { rho *= rho_update; }
  void set_rho0(const float_t rho0) { rho = rho0; }
};

} // namespace softcons
} // namespace mehrotra

#endif