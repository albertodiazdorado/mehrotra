#ifndef MATOPS_DOUBLE_HPP_
#define MATOPS_DOUBLE_HPP_

#include <algorithm>
#include <array>

#include "detail/matops.hpp"
#include "matrix/matrix.hpp"

extern "C" {
// Vector sum
void daxpy_(const int *, const double *, const double *, const int *, double *,
            const int *);
// Dot product
double ddot_(const int *, const double *, const int *, const double *,
             const int *);
// Positive definite matrix factorization
void dpptrf_(const char *, const int *, double *, int *);
void dpptri_(const char *, const int *, double *, int *);
// Matrix-vector product for symmetric, packed matrix
void dspmv_(const char *, const int *, const double *, const double *,
            const double *, const int *, const double *, double *, const int *);
// Matrix-vector product for general matrix
void dgemv_(const char *, const int *, const int *, const double *,
            const double *, const int *, const double *, const int *,
            const double *, double *, const int *);
// Backsolve for lower triangular coefficients matrix
void dtptrs_(const char *, const char *, const char *, const int *, const int *,
             const double *, double *, const int *, const int *);
// Get the coefficients for a Givens rotation
void dlartg_(const double *, const double *, double *, double *, double *);
// Execute Givens rotation
void drot_(const int *, double *, const int *, double *, const int *,
           const double *, const double *);
// Rank one update
void dspr_(const char *, const int *, const double *, const double *,
           const int *, double *);
}

namespace mehrotra {
namespace detail {

template <> struct MatOps<double> {

  /**
   * @brief Computes the vector addition y <- a*x + y
   *
   * @tparam n Vector size
   * @param a Scalar
   * @param x (Const) vector of size n
   * @param y Vector of size n
   */
  template <size_t n>
  static void vecsum(const double a, const std::array<double, n> &x,
                     std::array<double, n> &y) {
    constexpr int N{n}, inc{1};
    daxpy_(&N, &a, x.data(), &inc, y.data(), &inc);
  }

  /**
   * @brief Computes the dot product of two vectors
   *
   * Use dotproduct(x,x) to evaluate the square of the euclidean norm of a
   * vector.
   *
   * @tparam n Vector dimension
   * @param x First vector
   * @param y Second vector
   * @return double Dot product of input vectors
   */
  template <size_t n>
  static double dotproduct(const std::array<double, n> &x,
                           const std::array<double, n> &y) {
    constexpr int N{n}, inc{1};
    return ddot_(&N, x.data(), &inc, y.data(), &inc);
  }

  /**
   * @brief In place calculation of the cholesky factorization of a positive
   * definite matrix
   *
   * x <- cholfact(x)
   *
   * @tparam n Matrix dimension
   * @param x Positive definite matrix in packed format
   */
  template <size_t n> static void cholfact(mehrotra::PDMat<double, n> &x) {
    constexpr int N{n};
    constexpr char uplo{'L'};
    int info{0};
    dpptrf_(&uplo, &N, x.mat.data(), &info);
  }

  /**
   * @brief In place inversion of a diagonal matrix.
   *
   * Note that, despite the name, this function DOES NOT return the cholesky
   * factor of the supplied matrix. Rather than this, it return the square of
   * the Choleky factor.
   *
   * x <- cholfact(x)
   *
   * @tparam n Matrix dimension
   * @param x Positive definite matrix in packed format
   */
  template <size_t n> static void cholfact(mehrotra::DiagMat<double, n> &x) {
    std::transform(x.mat.cbegin(), x.mat.cend(), x.mat.begin(),
                   [](double x) { return 1 / x; });
  }

  /**
   * @brief In place cholesky factor rank-1 update
   *
   * Given a factorization A = L*L', and a rank-1 update A2 = A + x*x',
   * computes the factorization A2 = L2*L2' without explicitely computing A2
   *
   * @tparam n Matrix dimension
   * @param L Cholesky factor (lower triangular matrix in packed format)
   * @param x Rank-1 update vector
   */
  template <size_t n>
  static void cholupdate(mehrotra::PDMat<double, n> &L,
                         std::array<double, n> x) {
    constexpr int inc{1};
    double r{0}, c{0}, s{0};
    int skip{0}, N{n - 1};
    for (size_t col = 0; col < n; col++) {
      dlartg_(&L.mat[skip], &x[col], &c, &s, &r);
      L.mat[skip] = r;
      drot_(&N, &L.mat[skip + 1], &inc, &x[col + 1], &inc, &c, &s);
      N--;
      skip += n - col;
    }
  }

  template <size_t n>
  static void cholupdate2(mehrotra::PDMat<double, n> &L,
                          std::array<double, n> x) {
    double r{0}, c{0}, s{0};
    double temp1{0}, temp2{0};
    int skip{0};
    for (size_t col = 0; col < n; col++) {
      dlartg_(&L.mat[skip], &x[col], &c, &s, &r);
      L.mat[skip] = r;
      for (size_t row = col + 1; row < n; row++) {
        temp1 = L.mat[skip + row - col];
        temp2 = x[row];
        L.mat[skip + row - col] = c * temp1 + s * temp2;
        x[row] = -s * temp1 + c * temp2;
      }
      skip += n - col;
    }
  }

  /**
   * @brief Inversion of a positive definite matrix
   *
   * On input, L does not contain the positive definite matrix, but its cholesky
   * factorization. On output, it contains the inverse of the original positive
   * definite matrix.
   *
   * L <- inverse(L*L')
   *
   * @tparam n Matrix dimension
   * @param L PDMat that stores the cholesky factorization
   */
  template <size_t n> static void invert(PDMat<double, n> &L) {
    int info{0};
    constexpr char uplo{'L'};
    constexpr int N{n};
    dpptri_(&uplo, &N, L.mat.data(), &info);
  }

  /**
   * @brief Matrix-vector product for a positive definite matrix
   *
   * Computes y <- alpha*A*x + beta*y
   *
   * @tparam n Vector size
   * @param alpha Scalar
   * @param A Positive definite matrix in packed form (lower triangular)
   * @param x Vector of size n
   * @param beta Scalar
   * @param y Vector of size n
   */
  template <size_t n>
  static void matvec(const double alpha, const mehrotra::PDMat<double, n> &A,
                     const std::array<double, n> &x, const double beta,
                     std::array<double, n> &y) {
    constexpr char uplo{'L'};
    constexpr int N{n}, inc{1};
    dspmv_(&uplo, &N, &alpha, A.mat.data(), x.data(), &inc, &beta, y.data(),
           &inc);
  }

  /**
   * @brief Matrix-vector product for a general rectangular matrix
   *
   * Computes y <- alpha*A*x + beta*y
   *
   * @tparam m Number of rows
   * @tparam n Number of columns
   * @param trans Use 'T' for using the transpose of matrix A. Use 'N' otherwise
   * @param alpha Scalar
   * @param A Rectangular matrix of dimension (m x n)
   * @param x Vector of size n
   * @param beta Scalar
   * @param y Vector of size n
   */
  template <char t, size_t m, size_t n>
  static void matvec(const double alpha, const mehrotra::GeMat<double, m, n> &A,
                     const std::array<double, (t == 'N') ? n : m> &x,
                     const double beta,
                     std::array<double, (t == 'N') ? m : n> &y) {
    constexpr int M{m}, N{n}, lda{m}, inc{1};
    constexpr char trans{t};
    dgemv_(&trans, &M, &N, &alpha, A.mat.data(), &lda, x.data(), &inc, &beta,
           y.data(), &inc);
  }

  /**
   * @brief Matrix-vector product for a diagonal matrix
   *
   * Computes y <- alpha*A*x + beta*y
   *
   * @tparam n Matrix dimension
   * @param alpha Scalar
   * @param A Diagonal matrix of dimension n
   * @param x Vector of size n
   * @param beta Scalar
   * @param y Vector of dimension n
   */
  template <size_t n>
  static void matvec(const double alpha, const mehrotra::DiagMat<double, n> &A,
                     const std::array<double, n> &x, const double beta,
                     std::array<double, n> &y) {
    for (size_t idx = 0; idx < n; idx++) {
      y[idx] = alpha * A.mat[idx] * x[idx] + beta * y[idx];
    }
  }

  /**
   * @brief Computes the solution to the system Lx=b or L'x=b, where L is lower
   * triangular and b is a vector
   *
   * Computes x<-L\b or x<-L'\b
   *
   * @tparam n Matrix dimension (number of equations)
   * @param trans Use 'T' for using the transpose of matrix L. Use 'N' otherwise
   * @param L Lower-triangular matrix in packed format
   * @param b Vector of size n (right hand side)
   */
  template <size_t n>
  static void backsolve(const char trans, const mehrotra::PDMat<double, n> &L,
                        std::array<double, n> &b) {
    constexpr char uplo{'L'}, diag{'N'};
    constexpr int N{n}, nrhs{1};
    int info{0};
    dtptrs_(&uplo, &trans, &diag, &N, &nrhs, L.mat.data(), b.data(), &N, &info);
  }

  /**
   * @brief Computes the solution to the system LX=B or L'X=B for a multiple
   * right-hand side, where L is lwoer triangular and B is a matrix
   *
   * Computes X<-L\B of x<-L'\B
   *
   * @tparam n Matrix dimension (number of equations)
   * @tparam m Number of right-hand sides
   * @param trans Use 'T' for using the transpose of matrix L. Use 'N' otherwise
   * @param L Lower-triangular matrix in packed format
   * @param B Rectangular matrix of dimensions (n x m)
   */
  template <size_t n, size_t m>
  static void backsolve(const char trans, const mehrotra::PDMat<double, n> &L,
                        mehrotra::GeMat<double, n, m> &B) {
    constexpr char uplo{'L'}, diag{'N'};
    constexpr int N{n}, nrhs{m};
    int info{0};
    dtptrs_(&uplo, &trans, &diag, &N, &nrhs, L.mat.data(), B.mat.data(), &N,
            &info);
  }

  /**
   * @brief Add one vector to the diagonal of a positive definite matrix
   *
   * Computes A <- A + I*x, where I is the identity matrix
   *
   * @tparam n Matrix dimension
   * @param A Positive definite matrix in packed format (lower triangular)
   * @param x Vector of size n
   */
  template <size_t n>
  static void diagsum(mehrotra::PDMat<double, n> &A,
                      const std::array<double, n> &x) {
    size_t skip = 0;
    for (size_t idx = 0; idx < n; idx++) {
      A.mat[skip] += x[idx];
      skip += n - idx;
    }
  }

  /**
   * @brief Add two vectors to the diagonal of a positive definite matrix
   *
   * Computes A<-A+I*(x+y)
   *
   * @tparam n Matrix dimension
   * @param A Positive definite matrix in packed format (lower triangular)
   * @param x Vector of size n
   * @param y Vector of size n
   */
  template <size_t n>
  static void diagsum(mehrotra::PDMat<double, n> &A,
                      const std::array<double, n> &x,
                      const std::array<double, n> &y) {
    size_t skip = 0;
    for (size_t idx = 0; idx < n; idx++) {
      A.mat[skip] += x[idx] + y[idx];
      skip += n - idx;
    }
  }

  /**
   * @brief Add one vector to the diagonal of a diagonal matrix
   *
   * Computes A<-A + I*x, where I is the identity matrix
   *
   * @tparam n Matrix dimension
   * @param A Diagonal matrix of dimension n
   * @param x Vector of size n
   */
  template <size_t n>
  static void diagsum(mehrotra::DiagMat<double, n> &A,
                      const std::array<double, n> &x) {
    for (size_t idx = 0; idx < n; idx++) {
      A.mat[idx] += x[idx];
    }
  }

  /**
   * @brief Add two vectors to the diagonal of a diagonal matrix
   *
   * Computes A<-A+I*(x+y)
   *
   * @tparam n Matrix dimension
   * @param A Diagonal matrix of dimension n
   * @param x Vector of size n
   * @param y Vector of size n
   */
  template <size_t n>
  static void diagsum(mehrotra::DiagMat<double, n> &A,
                      const std::array<double, n> &x,
                      const std::array<double, n> &y) {
    for (size_t idx = 0; idx < n; idx++) {
      A.mat[idx] += x[idx] + y[idx];
    }
  }

  /**
   * @brief Rank-1 update of a positive definite matrix
   *
   * Computes A<-A + alpha*x*x'
   *
   * @tparam n Matrix dimension
   * @param alpha Scalar
   * @param x Vector of size n
   * @param A Positive definite matrix in packed format (lower triangular)
   */
  template <size_t n>
  static void rank1(const double alpha, mehrotra::PDMat<double, n> &A,
                    const std::array<double, n> &x) {
    constexpr char uplo{'L'};
    constexpr int N{n}, inc{1};
    dspr_(&uplo, &N, &alpha, x.data(), &inc, A.mat.data());
  }

  /**
   * @brief Rank-k update of a positive definite matrix
   *
   * Compute A<-A + alpha*X*X'
   *
   * @tparam n Matrix dimension
   * @tparam m Rank of the update matrix X*X'
   * @param alpha Scalar
   * @param X Rectangular matrix of dimension (n x m)
   * @param A Positive definite matrix in packed format (lower triangular)
   */
  template <size_t n, size_t m>
  static void rankk(const double alpha, mehrotra::PDMat<double, n> &A,
                    const mehrotra::GeMat<double, n, m> &X) {
    constexpr char uplo{'L'};
    constexpr int N{n}, inc{1};
    for (size_t idx = 0; idx < m; idx++) {
      dspr_(&uplo, &N, &alpha, X.mat.data() + idx * N, &inc, A.mat.data());
    }
  }

  /**
   * @brief Rank-k update of a positive definite matrix
   *
   * Compute A<-A + alpha*X*X', where alpha is a vector of coefficients
   *
   * @tparam n Matrix dimension
   * @tparam m Rank of the update matrix X*X'
   * @param alpha Vector
   * @param X Rectangular matrix of dimension (n x m)
   * @param A Positive definite matrix in packed format (lower triangular)
   */
  template <size_t n, size_t m>
  static void rankk(const std::array<double, m> &alpha,
                    mehrotra::PDMat<double, n> &A,
                    const mehrotra::GeMat<double, n, m> &X) {
    constexpr char uplo{'L'};
    constexpr int N{n}, inc{1};
    for (size_t idx = 0; idx < m; idx++) {
      dspr_(&uplo, &N, &alpha[idx], X.mat.data() + idx * N, &inc, A.mat.data());
    }
  }

  /**
   * @brief Rank-k update of a positive definite matrix
   *
   * Compute A<-A + alpha*X'*X
   *
   * @tparam n Matrix dimension
   * @tparam m Rank of the update matrix X'*X
   * @param alpha Scalar
   * @param X Rectangular matrix of dimension (m x n)
   * @param A Positive definite matrix in packed format (lower triangular)
   */
  template <size_t n, size_t m>
  static void rankkt(const double alpha, mehrotra::PDMat<double, n> &A,
                     const mehrotra::GeMat<double, m, n> &X) {
    constexpr char uplo{'L'};
    constexpr int N{n}, inc{m};
    for (size_t idx = 0; idx < m; idx++) {
      dspr_(&uplo, &N, &alpha, X.mat.data() + idx, &inc, A.mat.data());
    }
  }

  /**
   * @brief Rank-k update of a positive definite matrix
   *
   * Compute A<-A + alpha*X'*X, where alpha is a vector of coefficients
   *
   * @tparam n Matrix dimension
   * @tparam m Rank of the update matrix X'*X
   * @param alpha Scalar
   * @param X Rectangular matrix of dimension (m x n)
   * @param A Positive definite matrix in packed format (lower triangular)
   */
  template <size_t n, size_t m>
  static void rankkt(const std::array<double, m> &alpha,
                     mehrotra::PDMat<double, n> &A,
                     const mehrotra::GeMat<double, m, n> &X) {
    constexpr char uplo{'L'};
    constexpr int N{n}, inc{m};
    for (size_t idx = 0; idx < m; idx++) {
      dspr_(&uplo, &N, &alpha[idx], X.mat.data() + idx, &inc, A.mat.data());
    }
  }

  /**
   * @brief Addition of PDMat and PDMat
   *
   * Performs the operation A <- A+B, where A and B are positive definite
   * matrices in packed form
   *
   * @tparam n Matrix dimension
   * @param A Positive definite, packed matrix
   * @param B Positive definite, packed matrix
   */
  template <size_t n>
  static void matsum(PDMat<double, n> &A, const PDMat<double, n> &B) {
    vecsum(float{1}, B.mat, A.mat);
  }

  /**
   * @brief Addition of PDMat and PDMat
   *
   * Performs the operation A <- A+B, where A is positive definite (in packed
   * form) and B is diagonal
   *
   * @tparam n Matrix dimension
   * @param A Positive definite, packed matrix
   * @param B Positive definite, packed matrix
   */
  template <size_t n>
  static void matsum(PDMat<double, n> &A, const DiagMat<double, n> &B) {
    size_t skip{0};
    for (size_t idx = 0; idx < n; idx++) {
      A.mat[skip] += B.mat[idx];
      skip += n - idx;
    }
  }
};

} // namespace detail
} // namespace mehrotra

#endif