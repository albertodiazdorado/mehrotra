- [x] **ISSUE 1**:
Why not inverthing Phi at the end of the affine forward step?
In the wollowing, you only use Phi for products in the form Phi^{-1}\*vec, where using the explicit inverse is advantageous with respect to using the cholesky factorization.
In addition, that would allow you to use MatOps::matvec in a uniform way for DiagMat and PDMat, avoiding the complication inverse_matrix_multiply.
To make this modification, you must make sure that the cholesky factorization of Phi is not needed anymore after completition of the affine forward step.

    **ADDRESSED:** phi is inverted upon exit from the shur_complement method. This is true both for DiagMat and PDMat, though DiagMat is already inverted upon entrance to the shur_complement method.

- [x] **ISSUE 2**:
Right now, the aff_forward and cc_forward initialize the right-hand side vector rd.
However, they do not include the linear penalty terms r, q and p, which are part of the stationarity residual rc.
We shall decide how do we want to incorporate this feature:
  - [ ] Shall rd be initialized in the shell, and augmented in the constraint class?
  - [x] Shall the linear penalty term be passed to the constraint class, and rd can be then initialized there?

- [x] **ISSUE 3: Initialization of variables x and u.**
Right now, constraints take one step of the variablse x and u to initialize them.
However, the whole vector shall be initialized.
Signatures shall be changed to make this possible.

  **ADDRESSED:** Just one element in the vector of variables is passed. After that element has been initialized, it is copied into the remaining elements in the vector.
There was no need to change the signatures.

- [x] **ISSUE 4: Initialization of the slack variables.**
Right now, slack variables are initialized to 20.
However, it is more sensible (right?) to initialize them to a value consistent with the actual initial value of x and u.
It was done that way in the previous solver.

- [x] **ISSUE 5:**
MatOps::matvec does not work as expected with PDMat, when you provide x=y and beta=0.
My work-around was using an intermediate variable "stupid", but it is soooo ugly.
Verify that the problem is real.
If it is not, find a work-around.
If it is, find a work-around.

  **ADDRESSED:** The problem is that matvec takes vectors by reference. If the first and the second vectors have the same reference, the function does not work as expected. The same goes for the BLAS functions dspmv_ and sspmv_, which take pointers. If the two pointers point to the same memory location, the function does not work. This is how the BLAS functions are defined, and matvec just mimics all this. There is no workaround, apart from using a meaningful name instead of "stupid"

- [x] **ISSUE 6:**
The vectors s_cc and lambda_cc are, most likely, not needed.
At least, they are not needed for simple bounded variables.
Consider removing them, and also speed up calculations.

  ;**ADDRESSED:** 04/06/2018 -- Vectors lambda_cc and s_cc removed from the hardcons::simplebounds class. Algorithm works fine.

- [ ] **ISSUE 7:**
You are using booleans in arithmetic operations with integers.
You do it whenever you evaluate the duality gap (terminate) of the affine duality gap (centering).

- [x] **ISSUE 8:**
Whenever you pass u[0], or x[0], or x[T-1], to the constructor of the hardconstraints, that variable is uninitialized.
Every variable should be initialized before using it for first time.
Consider removing the initialization of u[0] - x[0] - x[T-1] from the constructors of hard constraints.
Better, initialize them to zeros in the shell and then pass them to the policies in order to have them initialized.

  **ADDRESSED:** 10/06/2018 -- Add new method (initial_value) that avoids the aforementioned complication.

- [ ] **ISSUE 9:**
The cholesky factor update is weirdly done.
One would like to have a rank-k cholesky factor update, not a dumb sequence of rank-1 updates.
Furthermore, one would like to be able to provide a scalar alpha, rather than the custom solution that I am using [multiply the vector by sqrt(alpha)].
Even if the current solution works, it is so ugly that it needs to be re-worked.

- [x] **ISSUE 10:**
The affine forward methods (in hardcons and softcons) need another argument, which may be std::true_type or std::false_type.
The distinction has no effect for unconstrained, simplebounds and fullbounds.
However, it is needed for linearbounds, to differentiate whether you should do constraint addition or cholesky factor update.
The boolean only_linear must be used to pass either std::true_type or std::false_type.

  **ADDRESSED:** 06/06/2018 -- Issue addressed

- [x] **ISSUE 11:**
The method softcons::simplebounds::afs(std::true_type) is not created yet.
The templated method will be used (which is wrong).
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**ADDRESSED:** simplebounds::afs needs not the std::true_type version, since the variable cannot be unconstrained if it is bounded.