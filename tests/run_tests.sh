#!/usr/bin/env bash

# Global variables
tests_dir=$(pwd)
compiled_test_name="test.out"
failed_tests_gcc=0
failed_tests_clang=0

# Conditional compile options
compile_gcc=1
compile_clang=1
compile_openblas=1
compile_custom_blas=1

# Global compile options
warnings="-Wall -Wpedantic"
includes="-I ../include -I ../gtest/googletest/include"
libraries="-L ../build_gtest/lib"
links="-lblas -llapack -lgtest"

function compile_test_gcc() {
    local file=$1
    local custom_blas=$2
    g++ ${file} -o ${tests_dir}/${compiled_test_name}   \
        -pthread -std=c++11                             \
        ${warnings} ${includes} ${links} ${libraries}   \
        -DMPC_GTEST_COMPILE ${custom_blas}
}

function compile_test_clang() {
    local file=$1
    local custom_blas=$2
    clang++ ${file} -o ${tests_dir}/${compiled_test_name}   \
        -pthread -std=c++11                                 \
        ${warnings} ${includes} ${links} ${libraries}       \
        -DMPC_GTEST_COMPILE ${custom_blas}
}

function compile_test(){
    if [[ $# -lt 2 ]]; then
        echo "compile_test expected at least 2 arguments. ${$#} arguments provided."
        exit 1
    fi
    local file=$1
    local compiler=$2
    local custom_blas=$3

    if [[ ${compiler} == "gcc" ]]; then
        compile_test_gcc ${file} ${custom_blas}
    else
        compile_test_clang ${file} ${custom_blas}
    fi
}

# Run tests with gcc
if [[ ${compile_gcc} == 1 ]]; then
    for file in ${tests_dir}/*; do
        if [[ -f ${file} ]] && [[ "${file}" == *_test.cpp ]]; then

            # Compile with openblas
            if [[ ${compile_openblas} == 1 ]]; then
                compile_test ${file} gcc
                if [[ $? -ne 0 ]]; then
                    echo "Failed to compile ${file} with gcc"
                    exit 2
                fi
                ./${compiled_test_name}
                failed_tests_gcc=$((failed_tests_gcc+$?))
            fi

            # Compile with custom blas
            if [[ ${compile_custom_blas} == 1 ]]; then
                compile_test ${file} gcc -DMPC_NO_BLAS
                if [[ $? -ne 0 ]]; then
                    echo "Failed to compile ${file} with gcc and custom blas installation"
                    exit 2
                fi
                ./${compiled_test_name}
                failed_tests_gcc=$((failed_tests_gcc+$?))
            fi
        fi
    done
fi

# Run tests with clang
if [[ ${compile_clang} == 1 ]]; then
    for file in ${tests_dir}/*; do
        if [[ -f ${file} ]] && [[ "${file}" == *_test.cpp ]]; then

            # Compile with openblas
            if [[ ${compile_openblas} == 1 ]]; then
                compile_test ${file} clang
                if [[ $? -ne 0 ]]; then
                    echo "Failed to compile ${file} with clang"
                    exit 3
                fi
                ./${compiled_test_name}
                failed_tests_clang=$((failed_tests_clang+$?))
            fi

            # Compile with custom blas
            if [[ ${compile_custom_blas} == 1 ]]; then
                compile_test ${file} clang -DMPC_NO_BLAS
                if [[ $? -ne 0 ]]; then
                    echo "Failed to compile ${file} with clang and custom blas installation"
                    exit 3
                fi
                ./${compiled_test_name}
                failed_tests_clang=$((failed_tests_clang+$?))
            fi
        fi
    done
fi

echo -e "\n\n\n***********************************"
echo "TEST REPORT:"
echo -e "\t${failed_tests_gcc} tests failed to run with GCC"
echo -e "\t${failed_tests_clang} tests failed to run with CLANG"

rm test.out

exit $(($failed_tests_gcc + $failed_tests_clang))