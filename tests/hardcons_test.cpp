#include <gtest/gtest.h>
#include <limits>

#include "mehrotra.hpp"
#include "ops.hpp"

using namespace mehrotra;

template <class float_t> class unconstrained : public ::testing::Test {
public:
  static constexpr size_t n{4}, k{0}, T{10};

  const std::array<float_t, n> lb{0}, ub{0};
  const std::array<float_t, k> f{};
  const GeMat<float_t, k, n> F{};
  hardcons::unconstrained<float_t, int, n, k, T> A, B, C, D;

  std::array<float_t, n> x{0};

  unconstrained() : A(), B{}, C(lb, ub, F, f), D{lb, ub, F, f} {
    C.initial_value(x);
    D.initial_value(x);
  }
  void SetUp() {}
  void TearDown() {}
  ~unconstrained() {}
};

template <class float_t> class simplebounds : public ::testing::Test {
public:
  static constexpr size_t n{4}, k{0}, T{6};

  std::array<float_t, n> x, x2;
  const std::array<float_t, n> lb, ub;
  const std::array<float_t, k> f{};
  const GeMat<float_t, k, n> F{};

  hardcons::simplebounds<float_t, int, n, k, T> A, B;

  const std::array<float_t, n> x0{1, 1, 2.5, 2.5};

  simplebounds()
      : x{0}, lb{0, 0, 0, 0}, ub{2, 2, 5, 5}, A{ub, lb, F, f}, B(ub, lb, F, f) {
    A.initial_value(x);
    B.initial_value(x2);
  }
  void SetUp() {}
  void TearDown() {}
  ~simplebounds() {}
};

typedef ::testing::Types<float, double> MyTypes;

TYPED_TEST_SUITE(unconstrained, MyTypes,);
TYPED_TEST_SUITE(simplebounds, MyTypes,);

TYPED_TEST(unconstrained, all) {
  Ops<TypeParam>::expect_equal(this->A.duality_gap(), 0);
  Ops<TypeParam>::expect_equal(this->A.affine_gap(6.66), 0);
}

TYPED_TEST(simplebounds, initialization) {
  for (size_t idx = 0; idx < TestFixture::n; idx++) {
    Ops<TypeParam>::expect_equal(this->x[idx], this->x0[idx]);
  }
}

TYPED_TEST(simplebounds, afs) {
  const std::array<TypeParam, TestFixture::n *(TestFixture::n + 1) / 2> p1{
      8, 1, -1, 5, 5, 1.5, 3, 4, 3, 9};
  PDMat<TypeParam, TestFixture::n> Phi{p1}, Phi2{p1};
  Phi2.mat[0] = 48;
  Phi2.mat[4] = 45;
  Phi2.mat[7] = 20;
  Phi2.mat[9] = 25;

  std::array<TypeParam, TestFixture::n> rd{1, 1, 1, 1};
  const std::array<TypeParam, TestFixture::n> rd2{rd};

  this->A.afs(this->x, Phi, rd, 2, true);

  for (size_t idx = 0; idx < Phi.mat.size(); idx++)
    Ops<TypeParam>::expect_equal(Phi.mat[idx], Phi2.mat[idx]);
  for (size_t idx = 0; idx < rd.size(); idx++)
    Ops<TypeParam>::expect_equal(rd[idx], rd2[idx]);

  const std::array<TypeParam, TestFixture::n> d1{5, 6, 5, 4},
      d2{45, 46, 21, 20};
  DiagMat<TypeParam, TestFixture::n> D1{d1};
  const DiagMat<TypeParam, TestFixture::n> D2{d2};

  this->A.afs(this->x, D1, rd, 2, true);
  for (size_t idx = 0; idx < D1.mat.size(); idx++)
    Ops<TypeParam>::expect_equal(D1.mat[idx], D2.mat[idx]);
  for (size_t idx = 0; idx < rd.size(); idx++)
    Ops<TypeParam>::expect_equal(rd[idx], rd2[idx]);
}

TYPED_TEST(simplebounds, affine_step) {
  const std::array<TypeParam, TestFixture::n *(TestFixture::n + 1) / 2> p1{
      8, 1, -1, 5, 5, 1.5, 3, 4, 3, 9};
  PDMat<TypeParam, TestFixture::n> Phi{p1}, Phi2{p1};
  Phi2.mat[0] = 48;
  Phi2.mat[4] = 45;
  Phi2.mat[7] = 20;
  Phi2.mat[9] = 25;

  std::array<TypeParam, TestFixture::n> rd{1, 1, 1, 1};
  const std::array<TypeParam, TestFixture::n> rd2{rd};

  this->A.afs(this->x, Phi, rd, 2, true);
  for (size_t idx = 0; idx < Phi.mat.size(); idx++)
    Ops<TypeParam>::expect_equal(Phi.mat[idx], Phi2.mat[idx]);
  for (size_t idx = 0; idx < rd.size(); idx++)
    Ops<TypeParam>::expect_equal(rd[idx], rd2[idx]);

  // Backsolving
  const std::array<TypeParam, TestFixture::n> xaff{0.7, 0.8, 4, -2};
  this->A.abs(this->x, xaff, 2);

  // Linesearch
  TypeParam sz{std::numeric_limits<TypeParam>::lowest()};
  this->A.linesearch(sz);
  sz = std::min(-sz, TypeParam{1});
  Ops<TypeParam>::expect_equal(sz, 0.38461538461538464);

  // Take step
  this->A.take_step(0.99 * sz);

  // Forward solve (next step)
  std::array<TypeParam, TestFixture::n *(TestFixture::n + 1) / 2> p3{p1};
  p3[0] = 77.72250969823484;
  p3[4] = 76.39390966772416;
  p3[7] = 45.19931949232924;
  p3[9] = 37.55756386708966;
  this->A.afs(this->x, Phi, rd, 2, true);
  for (size_t idx = 0; idx < Phi.mat.size(); idx++)
    Ops<TypeParam>::expect_equal(Phi.mat[idx], p3[idx]);
  const std::array<TypeParam, 4> rd3{-5.601952662721895, -6.545088757396453,
                                     -36.72544378698225, 19.86272189349112};
  for (size_t idx = 0; idx < rd.size(); idx++)
    Ops<TypeParam>::expect_equal(rd[idx], rd3[idx]);
}

TYPED_TEST(simplebounds, gaps) {
  const std::array<TypeParam, TestFixture::n *(TestFixture::n + 1) / 2> p1{
      8, 1, -1, 5, 5, 1.5, 3, 4, 3, 9};
  const std::array<TypeParam, TestFixture::n> xaff{0.7, 0.8, 4, -2};
  PDMat<TypeParam, TestFixture::n> Phi{p1};

  std::array<TypeParam, TestFixture::n> rd{1, 1, 1, 1};

  this->A.afs(this->x, Phi, rd, 2, true);
  this->A.abs(this->x, xaff, 2);
  TypeParam sz{std::numeric_limits<TypeParam>::lowest()};
  this->A.linesearch(sz);
  sz = std::min(-sz, TypeParam{1});
  Ops<TypeParam>::expect_equal(this->A.duality_gap(), 1680.0);
  Ops<TypeParam>::expect_equal(this->A.affine_gap(sz), 1518.284023668639);
  this->A.take_step(0.99 * sz);
}

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}