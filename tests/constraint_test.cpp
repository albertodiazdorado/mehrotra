#include <gtest/gtest.h>

#include "mehrotra.hpp"
#include "ops.hpp"

using namespace mehrotra;

template <class float_t> class test_input : public ::testing::Test {
public:
  static constexpr size_t nx{3}, nu{2}, k{0}, sk{0}, T{10};

  const DiagMat<float_t, nu> R{std::array<float_t, nu>{1, 2}};
  const PDMat<float_t, nu> Q{std::array<float_t, 3>{5.67, -0.1, 9.87}};
  const GeMat<float_t, nu, nx> B{
      std::array<float_t, nu * nx>{1, 2, 3, 4, 5, 6}};

  const std::array<float_t, nu> lb, ub;
  const std::array<float_t, k> f{};
  const GeMat<float_t, k, nu> F{};

  std::array<float_t, nu> u1{}, u2{};

  constraint<float_t, uint16_t, DiagMat, hardcons::unconstrained,
             softcons::unconstrained, nu, nx, T, k, sk, detail::input>
      Class1;
  constraint<float_t, uint16_t, DiagMat, hardcons::simplebounds,
             softcons::unconstrained, nu, nx, T, k, sk, detail::input>
      Class2;
  constraint<float_t, uint16_t, PDMat, hardcons::simplebounds,
             softcons::unconstrained, nu, nx, T, k, sk, detail::input>
      Class3;

  test_input()
      : lb{0, 0}, ub{5, 10}, Class1(B, R, ub, lb, F, f, lb, ub, F, f),
        Class2(B, R, ub, lb, F, f, lb, ub, F, f),
        Class3(B, Q, ub, lb, F, f, lb, ub, F, f) {
    Class1.initial_value(u1);
    Class2.initial_value(u2);
    Class3.initial_value(u2);
  }
  void SetUp() {}
  void TearDown() {}
  ~test_input() {}
};

template <class float_t> class test_state : public ::testing::Test {
public:
  static constexpr size_t nx{3}, nu{2}, k{0}, sk{0}, T{10};

  const DiagMat<float_t, nx> R{std::array<float_t, nx>{1, 2, 3.55}};
  const PDMat<float_t, nx> Q{
      std::array<float_t, 6>{5.67, 1.2, -1.2, 4.4, 0.1, 3}};
  const GeMat<float_t, nx, nx> B{
      std::array<float_t, nx * nx>{1, 2, 3, 4, 5, 6, -12, -4, -2}};

  const std::array<float_t, nx> lb, ub;
  const std::array<float_t, k> f{};
  const GeMat<float_t, k, nx> F{};

  std::array<float_t, nx> x1{}, x2{};

  constraint<float_t, uint16_t, DiagMat, hardcons::unconstrained,
             softcons::unconstrained, nx, nx, T - 1, k, sk, detail::state>
      Class1;
  constraint<float_t, uint16_t, DiagMat, hardcons::simplebounds,
             softcons::unconstrained, nx, nx, T - 1, k, sk, detail::state>
      Class2;
  constraint<float_t, uint16_t, PDMat, hardcons::simplebounds,
             softcons::unconstrained, nx, nx, T - 1, k, sk, detail::state>
      Class3;

  test_state()
      : lb{0, 0, -1}, ub{5, 10, 5}, Class1(B, R, ub, lb, F, f, lb, ub, F, f),
        Class2(B, R, ub, lb, F, f, lb, ub, F, f),
        Class3(B, Q, ub, lb, F, f, lb, ub, F, f) {
    Class1.initial_value(x1);
    Class2.initial_value(x2);
    Class3.initial_value(x2);
  }
  void SetUp() {}
  void TearDown() {}
  ~test_state() {}
};

typedef ::testing::Types<float, double> MyReals;

TYPED_TEST_SUITE(test_input, MyReals,);
TYPED_TEST_SUITE(test_state, MyReals,);

TYPED_TEST(test_input, constructor) {
  Ops<TypeParam>::expect_equal(this->u1[0], 0);
  Ops<TypeParam>::expect_equal(this->u1[1], 0);
  Ops<TypeParam>::expect_equal(this->u2[0], 2.5);
  Ops<TypeParam>::expect_equal(this->u2[1], 5);

  bool b1{this->Class1.has_no_cons}, b2{this->Class2.has_no_cons},
      b3{this->Class1.only_linear}, b4{this->Class1.only_linear};

  EXPECT_EQ(b1, true);
  EXPECT_EQ(b2, false);
  EXPECT_EQ(b3, false);
  EXPECT_EQ(b4, false);

  for (size_t idx = 0; idx < 2; idx++) {
    Ops<TypeParam>::expect_equal(this->Class1.phi.x.mat[idx],
                                 1 / this->R.mat[idx]);
  }

  for (size_t step = 0; step < 10; step++) {
    for (size_t idx = 0; idx < 2; idx++) {
      Ops<TypeParam>::expect_equal(this->Class2.phi[step].mat[idx], 0);
    }
  }
}

TYPED_TEST(test_state, constructor) {
  Ops<TypeParam>::expect_equal(this->x1[0], 0);
  Ops<TypeParam>::expect_equal(this->x1[1], 0);
  Ops<TypeParam>::expect_equal(this->x1[2], 0);
  Ops<TypeParam>::expect_equal(this->x2[0], 2.5);
  Ops<TypeParam>::expect_equal(this->x2[1], 5);
  Ops<TypeParam>::expect_equal(this->x2[2], 2);

  EXPECT_EQ(this->Class2.phi.size(), 9u);
  EXPECT_EQ(this->Class2.phi[0].mat.size(), 3u);

  bool b1{this->Class1.has_no_cons}, b2{this->Class2.has_no_cons},
      b3{this->Class1.only_linear}, b4{this->Class1.only_linear};

  EXPECT_EQ(b1, true);
  EXPECT_EQ(b2, false);
  EXPECT_EQ(b3, false);
  EXPECT_EQ(b4, false);

  for (size_t idx = 0; idx < 2; idx++) {
    Ops<TypeParam>::expect_equal(this->Class1.phi.x.mat[idx],
                                 1 / this->R.mat[idx]);
  }
  for (size_t step = 0; step < 10; step++) {
    for (size_t idx = 0; idx < 2; idx++) {
      Ops<TypeParam>::expect_equal(this->Class2.phi[step].mat[idx], 0);
    }
  }
}

TYPED_TEST(test_input, shur_complement) {
  this->Class2.phi[1] = std::array<TypeParam, 2>{25, 8.88};
  PDMat<TypeParam, 3> L{std::array<TypeParam, 6>{3, 1, 1, 4, 1, 5}};
  GeMat<TypeParam, 2, 3> B{std::array<TypeParam, 6>{5, 7, 8, 11, -1, -2}};
  const std::array<TypeParam, 6> r{9.518018018018019,   11.271171171171169,
                                   -0.7765765765765764, 20.186126126126123,
                                   -1.7974774774774769, 5.49045045045045};
  const std::array<TypeParam, 6> r2{19.018508609211683,  26.32829957738425,
                                    -3.1076443139056513, 44.051734521758874,
                                    -5.486294321644625,  6.0793386045943825};

  detail::MatOps<TypeParam>::cholfact(this->Class2.phi[1]);

  this->Class2.shur_complement(B, L, this->Class2.phi[1], this->Class2.phixt[1],
                               std::false_type{});
  Ops<TypeParam>::expect_equal(this->Class2.phi[1].mat[0], 1 / TypeParam{25});
  Ops<TypeParam>::expect_equal(this->Class2.phi[1].mat[1], 1 / 8.88);
  for (size_t idx = 0; idx < 6; idx++) {
    Ops<TypeParam>::expect_equal(L.mat[idx], r[idx]);
  }

  this->Class3.phi[2] = this->Q;
  detail::MatOps<TypeParam>::cholfact(this->Class3.phi[2]);
  this->Class3.shur_complement(B, L, this->Class3.phi[2], this->Class3.phixt[2],
                               std::false_type{});
  for (size_t idx = 0; idx < 6; idx++) {
    Ops<TypeParam>::expect_equal(L.mat[idx], r2[idx]);
  }
}

TYPED_TEST(test_state, shur_complement) {
  this->Class2.phi[5] = std::array<TypeParam, 3>{25, 1.88, 2.12};
  PDMat<TypeParam, 3> L{std::array<TypeParam, 6>{3, 1, 1, 4, 1, 5}}, L2;
  L2.mat.fill(-8);
  GeMat<TypeParam, 3, 3> A{
      std::array<TypeParam, 9>{5, 7, 8, 11, -1, -2, 3, 3, 4}},
      M;
  M.mat.fill(444);
  std::array<TypeParam, 6> r1{
      3.04, 1, 1, 4.531914893617021, 1, 5.471698113207547};
  std::array<TypeParam, 6> r2{57.25250903251706,   -9.070574066639903,
                              26.864552388598955,  7.258707346447209,
                              -4.0493295865114405, 12.694403853873947};
  std::array<TypeParam, 9> r3{0.2,  3.7234042553191493,  3.773584905660377,
                              0.44, -0.5319148936170213, -0.9433962264150942,
                              0.12, 1.595744680851064,   1.8867924528301885};

  detail::MatOps<TypeParam>::cholfact(this->Class2.phi[5]);
  this->Class2.shur_complement(A, L, L2, M, this->Class2.phi[5],
                               this->Class2.phixt[5], std::false_type{});

  Ops<TypeParam>::expect_equal(this->Class2.phi[5].mat[0], 1 / TypeParam{25});
  Ops<TypeParam>::expect_equal(this->Class2.phi[5].mat[1], 1 / TypeParam{1.88});
  Ops<TypeParam>::expect_equal(this->Class2.phi[5].mat[2], 1 / TypeParam{2.12});

  for (size_t idx = 0; idx < 6; idx++) {
    Ops<TypeParam>::expect_equal(L.mat[idx], r1[idx]);
    Ops<TypeParam>::expect_equal(L2.mat[idx], r2[idx]);
  }
  for (size_t idx = 0; idx < 9; idx++) {
    Ops<TypeParam>::expect_equal(M.mat[idx], r3[idx]);
  }

  this->Class3.phi[2] = this->Q;
  detail::MatOps<TypeParam>::cholfact(this->Class3.phi[2]);
  this->Class3.shur_complement(A, L, L2, M, this->Class3.phi[2],
                               this->Class3.phixt[2], std::false_type{});

  r1 = std::array<TypeParam, 6>{3.246599596198818,  0.9417323352646245,
                                1.0845820939707065, 4.775793264565891,
                                0.9685636550742208, 5.839912162293356};
  r2 = std::array<TypeParam, 6>{39.847689577449785, 6.8581354660551685,
                                19.99257557175146,  24.149816817113145,
                                4.920892247111286,  10.172406501543623};
  r3 = std::array<TypeParam, 9>{
      1.3017810796121128, 1.1643195135589794,  3.148568448059546,
      2.161699034980961,  -0.8219499931864424, 0.22541128043193256,
      0.7833241702731533, 0.43108673893736693, 1.6322934434780159};

  Ops<TypeParam>::expect_equal(L.mat, r1, 1e-10);
  Ops<TypeParam>::expect_equal(L2.mat, r2, 1e-10);
  Ops<TypeParam>::expect_equal(M.mat, r3, 1e-10);
}

TYPED_TEST(test_input, affine_forward) {
  this->u1 = std::array<TypeParam, 2>{2.5, 5};
  std::array<TypeParam, 3> nu{2.7, 3.7, -1}, nu_aff{-1, -1, -1};
  PDMat<TypeParam, 3> L{std::array<TypeParam, 6>{0.2, 0.2, 0.2, 0.4, 0.4, 0.8}};

  const std::array<TypeParam, 3> rnu_aff{
      -6.504705882352942, -12.674117647058825, -18.84352941176471};

  const std::array<TypeParam, 2> zeros{};

  this->Class2.aff_forward(this->R, zeros, this->B, this->u1, nu, L, nu_aff, 1);

  const std::array<TypeParam, 6> rL{0.6588235294117648, 1.1764705882352942,
                                    1.6941176470588237, 2.5294117647058822,
                                    3.682352941176471,  5.870588235294118};
  for (size_t idx = 0; idx < 6; idx++) {
    Ops<TypeParam>::expect_equal(L.mat[idx], rL[idx]);
  }
  for (size_t idx = 0; idx < 3; idx++) {
    Ops<TypeParam>::expect_equal(nu_aff[idx], rnu_aff[idx]);
  }
}

TYPED_TEST(test_state, affine_forward) {
  this->x1 = std::array<TypeParam, 3>{-0.8, 1.6, 3.2};
  std::array<TypeParam, 3> nu{2.7, 3.7, -1}, nu2{}, nu_aff{-1, -1, -1},
      nu_aff2{};
  PDMat<TypeParam, 3> L{std::array<TypeParam, 6>{0.2, 0.2, 0.2, 0.4, 0.4, 0.8}},
      L2{};
  L2.mat.fill(999);
  GeMat<TypeParam, 3, 3> M{};
  M.mat.fill(-1);
  this->Class2.aff_forward(this->R, std::array<TypeParam, 3>{}, this->B,
                           this->x1, nu, nu2, L, L2, M, nu_aff, nu_aff2, 8);

  const std::array<TypeParam, 6> rL{0.25882352941176473, 0.2, 0.2, 0.5, 0.4,
                                    0.8592300098716683};
  for (size_t idx = 0; idx < 6; idx++) {
    Ops<TypeParam>::expect_equal(L.mat[idx], rL[idx]);
  }

  const std::array<TypeParam, 6> rL2{0.9918936182567796,  2.3014342953370885,
                                     -1.8612624121711863, 5.573456825968295,
                                     -5.534289530224726,  10.307508274780789};
  for (size_t idx = 0; idx < 6; idx++) {
    Ops<TypeParam>::expect_equal(L2.mat[idx], rL2[idx]);
  }

  const std::array<TypeParam, 9> rM{
      0.058823529411764705, 0.2,  0.17769002961500494,
      0.23529411764705882,  0.5,  0.35538005923000987,
      -0.7058823529411764,  -0.4, -0.11846001974333663};
  for (size_t idx = 0; idx < 9; idx++) {
    Ops<TypeParam>::expect_equal(M.mat[idx], rM[idx]);
  }

  std::array<TypeParam, 3> rnu_aff{-8.970588235294118, -14.65,
                                   2.5751233958538995};
  for (size_t idx = 0; idx < 3; idx++) {
    Ops<TypeParam>::expect_equal(nu_aff[idx], rnu_aff[idx]);
  }

  rnu_aff = std::array<TypeParam, 3>{24.545218047732423, 78.68161256605308,
                                     -143.09681203182163};
}

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}