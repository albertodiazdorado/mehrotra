#include <gtest/gtest.h>

#include "mehrotra.hpp"

template <class T> struct Ops;

template <> struct Ops<float> {
  static void expect_equal(float x, float y) { EXPECT_FLOAT_EQ(x, y); }

  /**
   * @brief Compares whether two vectors are equal (against a given treshold)
   *
   * The function checks whether ||x-y|| / ||y|| < sqrt(diff), where ||x|| is
   * the 2-norm of vector x. The srqt() is included in the formula because the
   * 2-norm is not evaluated, but the square of the 2-norm, and hence square
   * roots must be taken at both sides of the inequality.
   *
   * @tparam n Scalar (vector dimension)
   * @param x First vector (the one whose correctnes you are checking)
   * @param y Second vector (the one you are comparing against)
   * @param diff Accuracy treshold, default to 1e-3
   */
  template <size_t n>
  static void expect_equal(const std::array<float, n> &x,
                           const std::array<float, n> &y,
                           const float diff = 1e-3) {
    std::array<float, n> r{};
    std::transform(x.cbegin(), x.cend(), y.cbegin(), r.begin(),
                   std::minus<float>());
    float denominator = mehrotra::detail::MatOps<float>::dotproduct(y, y);
    if (denominator < 1e-5)
      denominator = 1e-5;
    EXPECT_NEAR(mehrotra::detail::MatOps<float>::dotproduct(r, r) / denominator,
                0, diff);
  }
};

template <> struct Ops<double> {
  static void expect_equal(double x, double y) { EXPECT_DOUBLE_EQ(x, y); }

  /**
   * @brief Compares whether two vectors are equal (against a given treshold)
   *
   * The function checks whether ||x-y|| / ||y|| < sqrt(diff), where ||x|| is
   * the 2-norm of vector x. The srqt() is included in the formula because the
   * 2-norm is not evaluated, but the square of the 2-norm, and hence square
   * roots must be taken at both sides of the inequality.
   *
   * @tparam n Scalar (vector dimension)
   * @param x First vector (the one whose correctnes you are checking)
   * @param y Second vector (the one you are comparing against)
   * @param diff Accuracy treshold, default to 1e-6
   */
  template <size_t n>
  static void expect_equal(const std::array<double, n> &x,
                           const std::array<double, n> &y,
                           const float diff = 1e-3) {
    std::array<double, n> r{};
    std::transform(x.cbegin(), x.cend(), y.cbegin(), r.begin(),
                   std::minus<double>());
    double denominator = mehrotra::detail::MatOps<double>::dotproduct(y, y);
    if (denominator < 1e-5)
      denominator = 1e-5;
    EXPECT_NEAR(mehrotra::detail::MatOps<double>::dotproduct(r, r) /
                    denominator,
                0, diff);
  }
};