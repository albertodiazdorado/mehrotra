#include <gtest/gtest.h>

#define MPC_NU 2
#define MPC_NX 4
#define MPC_T 10
#define MPC_P PDMat

#include "mehrotra.hpp"

using namespace mehrotra;

template <class float_t> class mpc_data : public ::testing::Test {
public:
  static constexpr size_t nx{MPC_NX}, nu{MPC_NU}, T{MPC_T};

  std::array<float_t, nx * nx> A{1, 2,  3,  4,  5,  6,  7, 8,
                                  9, 10, 11, 12, 13, 14, 15};
  std::array<float_t, nu * nx> B{1, 0, 1, 0, 1, 0, 1, 0};

  std::array<float_t, nu> ubu{5, 5}, lbu{};
  std::array<float_t, nx> ubx{1, 1, 1, 1}, lbx{-1, -1, -1, -1};
  std::array<float_t, nx> ubt{ubx}, lbt{lbx};

  std::array<float_t, nu> R{0.5, 5};
  std::array<float_t, nx> Q{0.1, 1, 10, 100};
  std::array<float_t, nx * nx> P;

  mpcdata<float_t, uint16_t> MyMPC;

  mpc_data() : MyMPC{} {
    MyMPC.A = A;
    MyMPC.B = B;
    MyMPC.R = R;
    MyMPC.Q = Q;
    P.fill(9);
    MyMPC.P = P;
    MyMPC.ubu = ubu;
    MyMPC.lbu = lbu;
    MyMPC.ubx = ubx;
    MyMPC.lbx = lbx;
  }
  void SetUp() {}
  void TearDown() {}
  ~mpc_data() {}
};

typedef ::testing::Types<float, double> MyReals;

TYPED_TEST_SUITE(mpc_data, MyReals,);

TYPED_TEST(mpc_data, constructor) {
  for (size_t idx = 0; idx < MPC_NX; idx++) {
    EXPECT_EQ(this->MyMPC.ubt[idx], 0);
    EXPECT_EQ(this->MyMPC.lbt[idx], 0);
  }

  for (size_t idx = 0; idx < MPC_NX; idx++) {
    EXPECT_EQ(this->MyMPC.ubx[idx], this->ubx[idx]);
    EXPECT_EQ(this->MyMPC.lbx[idx], this->lbx[idx]);
  }

  this->MyMPC.ubt = this->lbt;
  for (size_t idx = 0; idx < MPC_NX; idx++) {
    EXPECT_EQ(this->MyMPC.ubt[idx], this->lbt[idx]);
    EXPECT_EQ(this->MyMPC.lbt[idx], 0);
  }
}

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}