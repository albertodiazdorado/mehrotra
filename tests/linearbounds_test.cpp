#include <gtest/gtest.h>

#define MPC_NU 2
#define MPC_NX 4
#define MPC_HL 10
#define MPC_KU 4
#define MPC_KX 8
#define MPC_KT 8
#define MPC_HCU linearbounds
#define MPC_HCX linearbounds
#define MPC_HCT linearbounds
#define MPC_P PDMat

#include "mehrotra.hpp"

#include "ops.hpp"

using namespace mehrotra;

template <class float_t> class data : public ::testing::Test {
public:
  static constexpr size_t nx{MPC_NX}, nu{MPC_NU}, T{MPC_HL};
  static constexpr size_t ku{MPC_KU}, kx{MPC_KX}, kt{MPC_KT};

  std::array<float_t, nx * nx> A{1,  2, 1, 1, -1, 4, 2, -5,
                                 10, 9, 1, 2, 0,  0, 1, -1};
  std::array<float_t, nu * nx> B{1, 0, 1, 0, 0, 1, 0, 1};
  std::array<float_t, nx> x0{0.5, 0.5, 0.5, 0.5};

  GeMat<float_t, ku, nu> Fu{
      std::array<float_t, ku * nu>{1, 0, -1, 0, 0, 1, 0, -1}};
  std::array<float_t, ku> fu{5, 5, 0, 0};

  GeMat<float_t, kx, nx> Fx{std::array<float_t, kx * nx>{
      1, 0, 0, 0, -1, 0, 0,  0, 0, 1, 0, 0, 0, -1, 0, 0,
      0, 0, 1, 0, 0,  0, -1, 0, 0, 0, 0, 1, 0, 0,  0, -1}},
      Ft{Fx};
  std::array<float_t, kx> fx{10, 10, 10, 10, 10, 10, 10, 10}, ft{fx};

  std::array<float_t, nu> R{0.5, 5};
  std::array<float_t, nx> Q{0.1, 1, 10, 100};
  std::array<float_t, nx * nx> P;

  mpcdata<float_t, uint16_t> MyData;

  data() {
    std::transform(A.cbegin(), A.cend(), A.begin(),
                   [](float_t a) { return a / 10; });
    MyData.A = A;
    MyData.B = B;
    MyData.R = R;
    MyData.Q = Q;
    P.fill(1);
    for (size_t idx = 0; idx < nx; idx++) {
      P[idx + nx * idx] += 8;
    }
    MyData.P = P;
    MyData.Fu = Fu;
    MyData.fu = fu;
    MyData.Fx = Fx;
    MyData.fx = fx;
    MyData.Ft = Ft;
    MyData.ft = ft;
  }
  void SetUp() {}
  void TearDown() {}
  ~data() {}
};

typedef ::testing::Types<float, double> MyTypes;

TYPED_TEST_SUITE(data, MyTypes,);

TYPED_TEST(data, empty_constructor) {
  solver<TypeParam, size_t> EmptyMPC;
  Ops<TypeParam>::expect_equal(EmptyMPC.fea_tol, 1e-2);
}

TYPED_TEST(data, constructor) {
  std::array<TypeParam, MPC_NU> u, uref{};
  std::array<TypeParam, MPC_NX> x, xref{};
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  for (size_t step = 0; step < MPC_HL; step++) {
    u = MyMPC.get_input(step);
    for (size_t idx = 0; idx < u.size(); idx++) {
      Ops<TypeParam>::expect_equal(u[idx], uref[idx]);
    }
  }
  for (size_t step = 0; step < MPC_HL; step++) {
    x = MyMPC.get_state(step);
    for (size_t idx = 0; idx < x.size(); idx++) {
      Ops<TypeParam>::expect_equal(x[idx], xref[idx]);
    }
  }
}

TYPED_TEST(data, initializer) {
  std::array<TypeParam, MPC_NU> u, uref{};
  std::array<TypeParam, MPC_NX> x, xref{};
  solver<TypeParam, uint16_t> MyMPC;
  MyMPC.initialize(this->MyData);
  for (size_t step = 0; step < MPC_HL; step++) {
    u = MyMPC.get_input(step);
    for (size_t idx = 0; idx < u.size(); idx++) {
      Ops<TypeParam>::expect_equal(u[idx], uref[idx]);
    }
  }
  for (size_t step = 0; step < MPC_HL; step++) {
    x = MyMPC.get_state(step);
    for (size_t idx = 0; idx < x.size(); idx++) {
      Ops<TypeParam>::expect_equal(x[idx], xref[idx]);
    }
  }
}

TYPED_TEST(data, eq_residual) {
  const std::array<TypeParam, MPC_NX> re0{0.25, 0, 1.1, 0}, re1{};

  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.equality_residual();
  auto re = MyMPC.nu_aff[0];
  Ops<TypeParam>::expect_equal(re, re0, 1e-8);
  for (size_t step = 1; step < MPC_HL; step++) {
    re = MyMPC.nu_aff[step];
    Ops<TypeParam>::expect_equal(re, re1, 1e-8);
  }
}

TYPED_TEST(data, affine_step) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.duality_gap();
  Ops<TypeParam>::expect_equal(MyMPC.mu, 153.33333333333334);
  MyMPC.equality_residual();
  MyMPC.affine_step();

  const std::array<TypeParam, MPC_NX> nu_aff{
      8.130421077203229, 14.256546472942935, 21.443514046624294,
      -2.4201914835831366};
  Ops<TypeParam>::expect_equal(MyMPC.nu_aff[0], nu_aff, 1e-8);

  const std::array<TypeParam, MPC_NU> u_aff{-0.09742724694474172,
                                            0.0336785323089269};
  Ops<TypeParam>::expect_equal(MyMPC.u_aff[0], u_aff);

  const std::array<TypeParam, MPC_NX> x_aff{
      0.15257275305525828, -0.09742724694474172, 1.1336785323089253,
      0.03367853230892586};
  Ops<TypeParam>::expect_equal(MyMPC.x_aff[0], x_aff);
}

TYPED_TEST(data, linesearch) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.equality_residual();
  MyMPC.duality_gap();
  MyMPC.affine_step();
  MyMPC.linesearch();
  Ops<TypeParam>::expect_equal(MyMPC.alpha, 0.8981757440706509);
}

TYPED_TEST(data, centering) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.equality_residual();
  MyMPC.duality_gap();
  MyMPC.affine_step();
  MyMPC.linesearch();
  MyMPC.centering();
  EXPECT_NEAR(MyMPC.mu_aff, 15.901527807906215, 1e-5);
  EXPECT_NEAR(MyMPC.sigma, 0.0011153388454504705, 1e-6);
}

TYPED_TEST(data, terminate) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  EXPECT_EQ(MyMPC.terminate(), false);
  EXPECT_EQ(MyMPC.k, 0u);
  MyMPC.k = 15;
  MyMPC.set_max_iterations(15);
  EXPECT_EQ(MyMPC.terminate(), true);
}

TYPED_TEST(data, full_stepsize) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.equality_residual();
  MyMPC.duality_gap();
  Ops<TypeParam>::expect_equal(MyMPC.mu, 153.33333333333334);

  MyMPC.affine_step();
  MyMPC.linesearch();
  MyMPC.centering();
  MyMPC.cc_step();
  MyMPC.linesearch();

  Ops<TypeParam>::expect_equal(MyMPC.alpha, 0.9470847718403174);

  MyMPC.take_step();

  const std::array<TypeParam, MPC_NX> nu{7.410402373316295, 14.27110256594312,
                                         16.256636229955898,
                                         0.3485332329600291};
  Ops<TypeParam>::expect_equal(MyMPC.nu[0], nu);

  const std::array<TypeParam, MPC_NX> x{
      0.18201740515239237, -0.052386075878086164, 1.0891543448432248,
      0.05777902830911965};
  Ops<TypeParam>::expect_equal(MyMPC.x[0], x);

  const std::array<TypeParam, MPC_NU> u{-0.05238607587808615,
                                        0.05777902830912093};
  Ops<TypeParam>::expect_equal(MyMPC.u[0], u);
}

TYPED_TEST(data, two_steps) {
  std::array<TypeParam, MPC_NX> nu, nu_aff;

  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.equality_residual();
  MyMPC.duality_gap();

  // Step one
  MyMPC.affine_step();
  MyMPC.linesearch();
  MyMPC.centering();
  MyMPC.cc_step();
  MyMPC.linesearch();
  MyMPC.take_step();
  MyMPC.terminate();

  nu_aff = std::array<TypeParam, MPC_NX>{
      0.015596518969521495, 1.3877787807814457e-17, 0.06862468346589612,
      1.2836953722228372e-15};
  Ops<TypeParam>::expect_equal(MyMPC.nu_aff[0], nu_aff);

  MyMPC.k = MyMPC.k + 1;

  nu = std::array<TypeParam, MPC_NX>{7.410402373316295, 14.27110256594312,
                                     16.256636229955898, 0.3485332329600291};
  Ops<TypeParam>::expect_equal(MyMPC.nu[0], nu);

  // Step two
  MyMPC.affine_step();

  nu = std::array<TypeParam, MPC_NX>{-0.1470663918600994, -0.5900148328292031,
                                     -0.006834037840930025, -2.641559817259979};
  Ops<TypeParam>::expect_equal(MyMPC.nu_aff[0], nu);

  nu = std::array<TypeParam, MPC_NX>{-1.6987080807110544, -1.5955490595835213,
                                     -2.203415570064095, -1.2642948861096976};
  Ops<TypeParam>::expect_equal(MyMPC.nu_aff[MPC_HL - 1], nu);

  MyMPC.linesearch();
  MyMPC.centering();
  MyMPC.cc_step();
  MyMPC.linesearch();
  MyMPC.take_step();
}

TYPED_TEST(data, terminate_side_calculations) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.equality_residual();
  MyMPC.duality_gap();

  MyMPC.affine_step();
  MyMPC.linesearch();
  MyMPC.centering();
  MyMPC.cc_step();
  MyMPC.linesearch();
  MyMPC.take_step();

  MyMPC.terminate();

  EXPECT_NEAR(MyMPC.mu, 9.800478893996411, 1e-3);

  std::array<TypeParam, MPC_NX> eq_res{
      0.015596518969521467, 0, 0.06862468346589568, 7.979727989493313e-16};
  Ops<TypeParam>::expect_equal(MyMPC.nu_aff[0], eq_res);
}

TYPED_TEST(data, solve) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.set_optimality_tolerance(1e-7);
  MyMPC.set_feasibility_tolerance(1e-7);
  MyMPC.solve();

  const std::array<TypeParam, MPC_NX> nu{5.3166845630249195, 8.9751729655173,
                                         14.53111728399386,
                                         -3.0965597381977465};
  Ops<TypeParam>::expect_equal(MyMPC.nu[0], nu);

  const std::array<TypeParam, MPC_NX> x{
      0.2500002350554479, 2.350554854076226e-7, 1.1000002863213076,
      2.863214727537976e-7};
  Ops<TypeParam>::expect_equal(MyMPC.x[0], x);

  const std::array<TypeParam, MPC_NU> u{7.077532485077268e-10,
                                        8.818519562921206e-10};
  Ops<TypeParam>::expect_equal(MyMPC.u[0], u);
}

TYPED_TEST(data, initializer_solve) {
  solver<TypeParam, uint16_t> MyMPC{};
  MyMPC.initialize(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.solve();

  const std::array<TypeParam, MPC_NX> x{
      0.2500002350554479, 2.350554854076226e-7, 1.1000002863213076,
      2.863214727537976e-7};
  Ops<TypeParam>::expect_equal(MyMPC.x[0], x);
}

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
