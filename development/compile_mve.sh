#!/usr/bin/env bash

mehrotra_dir=/home/alberto/Alberto/mehrotra

echo -e "Compile minimal working example...\n**********"
clang++ ${mehrotra_dir}/development/minimal_working_example.cpp \
    -Wall -Wpedantic                                            \
    -o ${mehrotra_dir}/development/mve.out                      \
    -I include -lblas -llapack

echo -e "\n\nExecute example...\n**********"
./development/mve.out